<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>LAMP STACK</title>
    <link rel="stylesheet" href="lamp.min.css">
</head>

<body>
    <section class="hero is-medium is-info is-bold">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    LAMP STACK
                </h1>
                <h2 class="subtitle">
                    Your local development environment!
                </h2>
            </div>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <h3 class="title is-3 has-text-centered">Environment</h3>
                    <hr>
                    <div class="content">
                        <ul>
                            <li>HOSTNAME <?= gethostname(); ?></li>
                            <li><?= apache_get_version(); ?></li>
                            <li>PHP <?= phpversion(); ?></li>
                            <li>
                                <?php
                                $link = mysqli_connect("database", "appuser", "apppass", null);

                                /* check connection */
                                if (mysqli_connect_errno()) {
                                    printf("MySQL connecttion failed: %s", mysqli_connect_error());
                                } else {
                                    /* print server version */
                                    printf("MySQL Server %s", mysqli_get_server_info($link));
                                }
                                /* close connection */
                                mysqli_close($link);
                                ?>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="column">
                    <h3 class="title is-3 has-text-centered">Quick Links</h3>
                    <hr>
                    <div class="content">
                        <ul>
                            <li><a href="http://localhost:<? print $_ENV['PMA_PORT']; ?>">phpMyAdmin</a></li>
                            <li>
                                <a href="#">Test DB Connection with mysqli</a><br>
                                <?php
                                $link = mysqli_connect("database", "appuser", "apppass", null);

                                if (!$link) {
                                    echo "Error: Unable to connect to MySQL." . PHP_EOL;
                                    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
                                    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
                                    exit;
                                }

                                echo "Success: A proper connection to MySQL was made! The docker database is great." . PHP_EOL;
                                echo "Host information: " . mysqli_get_host_info($link) . PHP_EOL;

                                mysqli_close($link);
                                ?>
                            </li>
                            <li>
                                <a href="#">Test DB Connection with PDO</a><br>
                                <?php

                                $DBuser = 'appuser';
                                $DBpass = 'apppass';
                                $pdo = null;

                                try {
                                    $database = 'mysql:host=database:3306';
                                    $pdo = new PDO($database, $DBuser, $DBpass);
                                    echo "Looking good, php connect to mysql successfully.";
                                } catch (PDOException $e) {
                                    echo "php connect to mysql failed with:\n $e";
                                }

                                $pdo = null;
                                ?>
                            </li>
                            <li><a href="#">phpinfo()</a></li>

                        </ul>
                    </div>
                </div>
            </div>
            <?php phpinfo() ?>
        </div>
    </section>
</body>

</html>